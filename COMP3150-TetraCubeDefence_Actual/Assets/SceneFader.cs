﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class SceneFader : MonoBehaviour
{
    public Image fade;
    public AnimationCurve fadeCurve;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(FadeIn());
    }

    public void FadeTo(string scene)
    {
        StartCoroutine(FadeOut(scene));
    }

    IEnumerator FadeIn()
    {
        float ti = 1f;

        while (ti > 0f)
        {
            ti -= Time.deltaTime;
            float a = fadeCurve.Evaluate(ti);
            fade.color = new Color (0f, 0f, 0f, a);
            yield return 0;
        }

    }

    IEnumerator FadeOut(string scene)
    {
        float ti = 0f;

        while (ti < 1f)
        {
            ti += Time.deltaTime;
            float a = fadeCurve.Evaluate(ti);
            fade.color = new Color(0f, 0f, 0f, a);
            yield return 0;
        }

        SceneManager.LoadScene(scene);
    }
}
