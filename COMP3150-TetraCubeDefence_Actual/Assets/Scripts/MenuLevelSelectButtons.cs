﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuLevelSelectButtons : MonoBehaviour
{
    public string loadLevel;

    private void OnMouseDown()
    {
        SceneManager.LoadScene(loadLevel);
    }
}
