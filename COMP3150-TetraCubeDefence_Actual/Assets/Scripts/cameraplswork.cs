﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*// code sourced from:
//https://stackoverflow.com/questions/58061623/rotating-camera-around-sphere-in-unity-c-using-arrow-keys
*/

public class cameraplswork : MonoBehaviour
{
        public GameObject cameraRotate;
    //All object are referenced using the inspector

    public float cameraAngularVelocity = 60f;
    public float cameraDistance = 200f;
    public float cameraAngleY = 0;
    public float cameraAngleX = 0;

    public float fovMin;
    public float fovMax;
    public float zoomSpeed = 4f;

    public GameObject cam1;
    public GameObject cam2;
    public GameObject cam3;
    public GameObject cam4;
    public GameObject cam5;
    public GameObject cam6;
    public GameObject cam7;
    public GameObject cam8;

    private Camera mainCam;

    void Start()
    {
        mainCam = Camera.main;
    }

    void Update()
    {
        /*if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            RaycastHit hit;
            Ray ray = this.transform.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            Vector3 desiredPosition;

            if (Physics.Raycast(ray, out hit))
            {
                desiredPosition = hit.point;
            }
            else
            {
                desiredPosition = transform.position;
            }
            float distance = Vector3.Distance(desiredPosition, transform.position);
            Vector3 direction = Vector3.Normalize(desiredPosition - transform.position) * (distance * Input.GetAxis("Mouse ScrollWheel"));

            transform.position += direction;
        }*/





        float angleDelta = cameraAngularVelocity * Time.deltaTime;

        //Standard Input management
        if (Input.GetKey("up"))
        {
            cameraAngleX += angleDelta;
        }
        if (Input.GetKey("down"))
        {
            cameraAngleX -= angleDelta;
        }
        if (Input.GetKey("right"))
        {
            cameraAngleY -= angleDelta;
        }
        if (Input.GetKey("left"))
        {
            cameraAngleY += angleDelta;
        }

        //Protections
        cameraAngleX = Mathf.Clamp(cameraAngleX, -90f, 90f);
        cameraAngleY = Mathf.Repeat(cameraAngleY, 360f);

        Quaternion cameraRotation = Quaternion.AngleAxis(cameraAngleY, Vector3.up) * Quaternion.AngleAxis(cameraAngleX, Vector3.right);

        Vector3 cameraPosition = cameraRotate.transform.position + cameraRotation * Vector3.back * cameraDistance;

        mainCam.transform.position = cameraPosition;
        mainCam.transform.rotation = cameraRotation;

        /*if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            mainCam.fieldOfView += zoomSpeed;
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            mainCam.fieldOfView -= zoomSpeed;
        }
        mainCam.fieldOfView = Mathf.Clamp(mainCam.fieldOfView, fovMin, fovMax);*/

        if (Input.GetKey(KeyCode.Tab)) {
            cam1.GetComponent<Camera>().enabled = true;
            cam2.GetComponent<Camera>().enabled = false;
            cam3.GetComponent<Camera>().enabled = false;
            cam4.GetComponent<Camera>().enabled = false;
            cam5.GetComponent<Camera>().enabled = false;
            cam6.GetComponent<Camera>().enabled = false;
            cam7.GetComponent<Camera>().enabled = false;
            cam8.GetComponent<Camera>().enabled = false;
        }

        if (Input.GetKey("1"))
        {
            cam1.GetComponent<Camera>().enabled = false;
            cam2.GetComponent<Camera>().enabled = true;
            cam3.GetComponent<Camera>().enabled = false;
            cam4.GetComponent<Camera>().enabled = false;
            cam5.GetComponent<Camera>().enabled = false;
            cam6.GetComponent<Camera>().enabled = false;
            cam7.GetComponent<Camera>().enabled = false;
            cam8.GetComponent<Camera>().enabled = false;
        }

        if (Input.GetKey("2"))
        {
            cam1.GetComponent<Camera>().enabled = false;
            cam2.GetComponent<Camera>().enabled = false;
            cam3.GetComponent<Camera>().enabled = true;
            cam4.GetComponent<Camera>().enabled = false;
            cam5.GetComponent<Camera>().enabled = false;
            cam6.GetComponent<Camera>().enabled = false;
            cam7.GetComponent<Camera>().enabled = false;
            cam8.GetComponent<Camera>().enabled = false;
        }

        if (Input.GetKey("3"))
        {
            cam1.GetComponent<Camera>().enabled = false;
            cam2.GetComponent<Camera>().enabled = false;
            cam3.GetComponent<Camera>().enabled = false;
            cam4.GetComponent<Camera>().enabled = true;
            cam5.GetComponent<Camera>().enabled = false;
            cam6.GetComponent<Camera>().enabled = false;
            cam7.GetComponent<Camera>().enabled = false;
            cam8.GetComponent<Camera>().enabled = false;
        }

        if (Input.GetKey("4"))
        {
            cam1.GetComponent<Camera>().enabled = false;
            cam2.GetComponent<Camera>().enabled = false;
            cam3.GetComponent<Camera>().enabled = false;
            cam4.GetComponent<Camera>().enabled = false;
            cam5.GetComponent<Camera>().enabled = true;
            cam6.GetComponent<Camera>().enabled = false;
            cam7.GetComponent<Camera>().enabled = false;
            cam8.GetComponent<Camera>().enabled = false;
        }

        if (Input.GetKey("5"))
        {
            cam1.GetComponent<Camera>().enabled = false;
            cam2.GetComponent<Camera>().enabled = false;
            cam3.GetComponent<Camera>().enabled = false;
            cam4.GetComponent<Camera>().enabled = false;
            cam5.GetComponent<Camera>().enabled = false;
            cam6.GetComponent<Camera>().enabled = true;
            cam7.GetComponent<Camera>().enabled = false;
            cam8.GetComponent<Camera>().enabled = false;
        }

        if (Input.GetKey("6"))
        {
            cam1.GetComponent<Camera>().enabled = false;
            cam2.GetComponent<Camera>().enabled = false;
            cam3.GetComponent<Camera>().enabled = false;
            cam4.GetComponent<Camera>().enabled = false;
            cam5.GetComponent<Camera>().enabled = false;
            cam6.GetComponent<Camera>().enabled = false;
            cam7.GetComponent<Camera>().enabled = true;
            cam8.GetComponent<Camera>().enabled = false;
        }

        if (Input.GetKey("7"))
        {
            cam1.GetComponent<Camera>().enabled = false;
            cam2.GetComponent<Camera>().enabled = false;
            cam3.GetComponent<Camera>().enabled = false;
            cam4.GetComponent<Camera>().enabled = false;
            cam5.GetComponent<Camera>().enabled = false;
            cam6.GetComponent<Camera>().enabled = false;
            cam7.GetComponent<Camera>().enabled = false;
            cam8.GetComponent<Camera>().enabled = true;
        }
    }
}
