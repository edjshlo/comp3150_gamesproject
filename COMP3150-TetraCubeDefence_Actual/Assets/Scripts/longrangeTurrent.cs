﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class longrangeTurrent : MonoBehaviour
{
	
	public float targetRange = 30; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDrawGizmosSelected() { 

    	Gizmos.color = Color.red; 
    	Gizmos.DrawWireSphere(transform.position, targetRange);  

    }
}
