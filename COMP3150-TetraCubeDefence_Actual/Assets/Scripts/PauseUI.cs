﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PauseUI : MonoBehaviour
{
    public GameObject pauseUI;
    public string loadMenu = "MainMenu";

    public SceneFader fader;
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Toggle();
        }
    }

    public void Toggle()
    {
        if (gameManager.gameEnded)
            return;

        pauseUI.SetActive(!pauseUI.activeSelf);

        if (pauseUI.activeSelf)
        {
            Time.timeScale = 0f;
        } else
        {
            Time.timeScale = 1f;
        }
    }

    public void RetryPause()
    {
        Toggle();
        fader.FadeTo(SceneManager.GetActiveScene().name);
    }

    public void MainMenuPause()
    {
        Toggle();
        fader.FadeTo(loadMenu);
    }
}
