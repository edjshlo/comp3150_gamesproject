﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

public class gameManager : MonoBehaviour
{
    public static bool gameEnded = false;
    public GameObject gameOverUI;
    public string loadMenu = "MainMenu";

    public SceneFader fader;

    void Start()
    {
        gameEnded = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(gameEnded)
            return;

     if (playerStats.lives <= 0)
        {
            endGame();
        }   
    }

    void endGame()
    {
        gameEnded = true;
        gameOverUI.SetActive(true);
        Time.timeScale = 0f;
    }

    public void RetryGameOver()
    {
        fader.FadeTo(SceneManager.GetActiveScene().name);
        Time.timeScale = 1f;
    }

    public void MainMenu()
    {
        fader.FadeTo(loadMenu);
    }
}
