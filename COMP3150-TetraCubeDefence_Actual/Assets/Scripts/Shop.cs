﻿using UnityEngine;

public class Shop : MonoBehaviour
{

    public TowerBlueprint standardTower;
    public TowerBlueprint LRTower;
    public TowerBlueprint SlowTower;
    
    BuildManager buildManager;

    void Start()
    {
        buildManager = BuildManager.instance;
    }

    public void SelectBasicTower()
    {
        buildManager.selectTurretToBuild(standardTower); 
    }

    //if we want to build another tower/turret
    public void SelectLRTower()
    {
        buildManager.selectTurretToBuild(LRTower);
    }

    //if we want to build another tower/turret
    public void SelectSlowTower()
    {
        buildManager.selectTurretToBuild(SlowTower);
    }

    //if we want to build another tower/turret
    /*public void SelectAnotherTower()
    {
        buildManager.selectTurretToBuild(buildManager.standardTurretPrefab);
    }*/
}
