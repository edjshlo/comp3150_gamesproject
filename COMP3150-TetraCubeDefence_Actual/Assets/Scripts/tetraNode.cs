﻿using UnityEngine.EventSystems;
using UnityEngine;
using System.Security.Cryptography;

public class tetraNode : MonoBehaviour
{
    public Color hoverColor;
    public Color noMoneyColor;

    //positioning the turret on the correct vectors
    public Vector3 positionOffset;

    

    //so if we want to place a tower before the level starts
    [Header("Optional")]
    public GameObject tower;
    private Renderer rend;
    private Color startColor;

    BuildManager buildManager;
    void Start()
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.GetColor("_Color");

        buildManager = BuildManager.instance;
    }

    public Vector3 getBuildPosition()
    {
        return transform.position + positionOffset;
    }

    void OnMouseDown()
    {

        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        if (!buildManager.canBuild)
        {
            return;
        }

        if (tower != null)
        {
            Debug.Log("Can't build here! - TODO if we want, we can display this on screen, depends what we decide on lol");
            return;
        }

        buildManager.buildTowerOn(this);
    }

    void OnMouseEnter()
    {

        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        if (!buildManager.canBuild)
        {
            return;
        }

        //if we have the money, put it as hover color.
        //If no money left for towers, put a color to show that the player has no money to place things
        if (buildManager.hasMoney)
        {
            rend.material.SetColor("_Color", hoverColor);
        } else
        {
            rend.material.SetColor("_Color", noMoneyColor);
        }
    }

    void OnMouseExit()
    {
        rend.material.SetColor("_Color", startColor);
    }
}
