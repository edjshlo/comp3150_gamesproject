﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    private Transform target;
    public float speed = 70f;
    public float explosionRadius = 0f; 
    public Wave EnemyCost;
    // public float bulletSpeed = 1f;

    public GameObject impactEffect; 

    public void seekEnemy(Transform enemyTarget)
    {

        target = enemyTarget;

    }

    // Update is called once per frame
    void Update()
    {

        if(target == null)
        {

            Destroy(gameObject);
            return;

        }

        Vector3 direction = target.position - transform.position;
        float distanceInCurrentFrame = speed * Time.deltaTime;

        if(direction.magnitude <= distanceInCurrentFrame)
        {

            targetHit();
            return;

        }

        transform.Translate(direction.normalized * distanceInCurrentFrame, Space.World);
        transform.LookAt(target); 
        
    }

    public void targetHit()
    {

        GameObject bulletEffectInstance = Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(bulletEffectInstance, 2f);

        if(explosionRadius > 0f) { 

            Explode(); 
            
            } else { 

                Damage(target);

            }
        // WaveSpawner.liveEnemies--;
        // playerStats.money += 5; 
         //Not working at the moment, enemycost.costOfenemy returns 0
        Destroy(target.gameObject);
        Destroy(gameObject);
        
    }

    void Explode() { 

        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius); 
        foreach (Collider collider in colliders) { 

            if(collider.tag == "Enemy") { 
                Damage(collider.transform);
                // WaveSpawner.liveEnemies--;
                // playerStats.money++;
             }
        }


    }

    void Damage (Transform enemy) { 

        WaveSpawner.liveEnemies--;
        //playerStats.money++;
        Destroy(enemy.gameObject);

    }

    void OnDrawGizmosSelected() { 

        Gizmos.color = Color.red; 
        Gizmos.DrawWireSphere(transform.position, explosionRadius); 

    }
}
