﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine;

public class AudioFader : MonoBehaviour
{
    public AudioSource audio;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(AudioFadeIn());
    }

    public void AudioFadeTo()
    {
        StartCoroutine(AudioFadeOut());
    }

    IEnumerator AudioFadeIn()
    {

        while (audio.volume < 1f)
        {
            audio.volume += Time.deltaTime;
            yield return 0;
        }

    }

    IEnumerator AudioFadeOut()
    {

        while (audio.volume > 0)
        {
            audio.volume -= Time.deltaTime;
            yield return 0;
        }

        audio.volume = 0;
        audio.Stop();
    }
}
