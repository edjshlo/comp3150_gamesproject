﻿using System.Collections.Specialized;
using System.IO.MemoryMappedFiles;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject cameraRotate;
    public float cameraAngularVelocity = 60f;
    public float cameraDistance = 200f;
    public float cameraAngleY = 0;
    public float cameraAngleX = 0;

    public Transform[] cams;
    public float speed;
    Transform currentView;

    public float zoomSpeed;
    public float orthographicSizeMin;
    public float orthographicSizeMax;
    public float fovMin;
    public float fovMax;
    //private Camera mainCam;


    void Start()
    {
        currentView = cams[0];
        //mainCam = Camera.main;
        cams[0].GetComponent<Camera>();

        cams[0].GetComponent<Camera>().enabled = true;
        cams[1].GetComponent<Camera>().enabled = false;
        cams[2].GetComponent<Camera>().enabled = false;
        cams[3].GetComponent<Camera>().enabled = false;
        cams[4].GetComponent<Camera>().enabled = false;
        cams[5].GetComponent<Camera>().enabled = false;
        cams[6].GetComponent<Camera>().enabled = false;
        cams[7].GetComponent<Camera>().enabled = false;
    }

    void Update()
    {

        float angleDelta = cameraAngularVelocity * Time.deltaTime;

        //Standard Input management
        if (Input.GetKey("w"))
        {
            cameraAngleX += angleDelta;
        }
        if (Input.GetKey("s"))
        {
            cameraAngleX -= angleDelta;
        }
        if (Input.GetKey("d"))
        {
            cameraAngleY -= angleDelta;
        }
        if (Input.GetKey("a"))
        {
            cameraAngleY += angleDelta;
        }

        //Protections
        //cameraAngleX = Mathf.Clamp(cameraAngleX, -90f, 90f);
        //cameraAngleY = Mathf.Repeat(cameraAngleY, 360f);

        Quaternion cameraRotation = Quaternion.AngleAxis(cameraAngleY, Vector3.up) * Quaternion.AngleAxis(cameraAngleX, Vector3.right);

        Vector3 cameraPosition = cameraRotate.transform.position + cameraRotation * Vector3.back * cameraDistance;

        cams[0].transform.position = cameraPosition;
        cams[0].transform.rotation = cameraRotation;

        if (Input.GetKey(KeyCode.Tab))
        {
            currentView = cams[0];
        }

        //cam1
        if (Input.GetKey("1"))
        {
            currentView = cams[1];
        }

        /*if (currentView == cams[1])
        {
            if (Input.GetKeyUp("right"))
            {
                currentView = cams[2];
            }

            if (Input.GetKeyUp("down"))
            {
                currentView = cams[3];
            }

        }*/

        //cam2
        if (Input.GetKey("2"))
        {
            currentView = cams[2];
        }

        /*if (currentView == cams[2])
        {
            if (Input.GetKeyUp("left"))
            {
                currentView = cams[1];
            }

            if (Input.GetKeyUp("down"))
            {
                currentView = cams[3];
            }

        }*/

        //cam3
        if (Input.GetKey("3"))
        {
            currentView = cams[3];
        }

        /*if (currentView == cams[3])
        {
            if (Input.GetKeyUp("left"))
            {
                currentView = cams[1];
            }

            if (Input.GetKeyUp("up"))
            {
                currentView = cams[2];
            }

            //since 3 has the ability to go down to 4, plane 1 and 2 completely
            //skip plane 3 when going down, and goes straight to 4...hmmmmm
            if (Input.GetKeyDown("down"))
            {
                currentView = cams[4];
            }
        }*/

        //cam4
        if (Input.GetKey("4"))
        {
            currentView = cams[4];
        }

        /*if (currentView == cams[4])
        {
            if (Input.GetKeyUp("up"))
            {
                currentView = cams[3];
            }

            if (Input.GetKeyUp("right"))
            {
                currentView = cams[5];
            }
        }*/

        //cam5
        if (Input.GetKey("5"))
        {
            currentView = cams[5];
        }

        /*if (currentView == cams[5])
        {
            if (Input.GetKeyDown("left"))
            {
                currentView = cams[4];
            }

            if (Input.GetKeyDown("right"))
            {
                currentView = cams[6];
            }
        }*/

        //cam6
        if (Input.GetKey("6"))
        {
            currentView = cams[6];
        }

        /*if (currentView == cams[6])
        {
            if (Input.GetKeyDown("left"))
            {
                currentView = cams[5];
            }

            if (Input.GetKeyUp("right"))
            {
                currentView = cams[7];
            }
        }*/

        //cam7
        if (Input.GetKey("7"))
        {
            currentView = cams[7];
        }

        /*if (currentView == cams[7])
        {
            if (Input.GetKeyDown("left"))
            {
                currentView = cams[6];
            }
        }*/

        /*//zoom in
        if (cams[0].orthographic)
        {
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                cams[0].orthographicSize += zoomSpeed;
            }
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                cams[0].orthographicSize -= zoomSpeed;
            }
            cams[0].orthographicSize = Mathf.Clamp(cams[0].orthographicSize, orthographicSizeMin, orthographicSizeMax);
        }
        else
        {
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                cams[0].fieldOfView += zoomSpeed;
            }
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                cams[0].fieldOfView -= zoomSpeed;
            }
            cams[0].fieldOfView = Mathf.Clamp(cams[0].fieldOfView, fovMin, fovMax);
        }*/

        /*if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            RaycastHit hit;
            Ray ray = this.transform.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            Vector3 desiredPosition;

            if (Physics.Raycast(ray, out hit))
            {
                desiredPosition = hit.point;
            }
            else
            {
                desiredPosition = transform.position;
            }
            float distance = Vector3.Distance(desiredPosition, transform.position);
            Vector3 direction = Vector3.Normalize(desiredPosition - transform.position) * (distance * Input.GetAxis("Mouse ScrollWheel"));

            transform.position += direction;
        }*/
    }
   
    void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, currentView.position, Time.deltaTime * speed);

        transform.rotation = Quaternion.Lerp(transform.rotation, currentView.rotation, Time.deltaTime * speed);
    }
}
