using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretScript : MonoBehaviour
{

	public Transform target; 
    private Enemy enemyTarget; 

    [Header("Turret Attributes")]
	public float targetRange = 10f; 

    [Header("For Bullets")]
    public float fireRate = 1f; 
    private float countdownToFire = 0f;
    public GameObject turretBullet; 

    [Header("For Laser")]
    public bool useLaser = false; 

    public float SlowPercentage = 0.5f; 
    public LineRenderer laserRenderer;
    public ParticleSystem laserImpactEffect;
    public Light laserImpactLight;

    [Header("Unity Setup Fields")]
	public string enemyTag = "Enemy";
	//public Transform rotatingPart;
    public float rotateSpeed = 10f; 
    
    public Transform firingPoint;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating ("updateTarget", 0f, 0.5f);
    }

    void updateTarget() { 

    	GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
    	float shortestDistance = Mathf.Infinity; 
    	GameObject nearestEnemy = null;

    	foreach(GameObject enemy in enemies) { 

    		float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
    		if(distanceToEnemy < shortestDistance) {

    			shortestDistance = distanceToEnemy;
    			nearestEnemy = enemy; 

    		}

    	}

    	if(nearestEnemy != null && shortestDistance <= targetRange) { 

    		target = nearestEnemy.transform; 
            enemyTarget = nearestEnemy.GetComponent<Enemy>();

    	} else { 

            target = null; 

        }

    }

    // Update is called once per frame
    void Update()
    {

    	if(target == null) { 

            if(useLaser == true) { 

                if(laserRenderer.enabled) { 

                    laserRenderer.enabled = false; 
                    laserImpactEffect.Stop();
                    laserImpactLight.enabled = false;

                }

            }

    		return; 

        }

            if(useLaser) { 

              Laser();   

            } else { 

                if(countdownToFire <= 0f) { 

                    turretFire(); 
                    countdownToFire = 1f/ fireRate; 

                }

                countdownToFire -= Time.deltaTime;

            }

        //Turret lock on (turn on if needed really)
        // Vector3 direction = target.position - transform.position; 
        // Quaternion lookRotation = Quaternion.LookRotation(direction);
        // Vector3 Rotation = Quaternion.Lerp(rotatingPart.rotation, lookRotation, Time.deltaTime * rotateSpeed).eulerAngles;
        // rotatingPart.rotation = Quaternion.Euler(0f, Rotation.y, 0f); 
        
    }

    void Laser() { 


        enemyTarget.Slow(SlowPercentage); 

        if(!laserRenderer.enabled) { 
            laserRenderer.enabled = true; 
            laserImpactEffect.Play();
            laserImpactLight.enabled = true;
        }

        laserRenderer.SetPosition(0, firingPoint.position); 
        laserRenderer.SetPosition(1, target.position);

        Vector3 dir = firingPoint.position - target.position; 

        laserImpactEffect.transform.position = target.position + dir.normalized;
        laserImpactEffect.transform.rotation = Quaternion.LookRotation(dir);

    }

    void turretFire() { 

        GameObject fireBullet = (GameObject)Instantiate(turretBullet, firingPoint.position, firingPoint.rotation);
        Bullet bullet = fireBullet.GetComponent<Bullet>(); 

        if(bullet != null)
        {

            bullet.seekEnemy(target);

        }
    }

    void OnDrawGizmosSelected() { 

    	Gizmos.color = Color.red; 
    	Gizmos.DrawWireSphere(transform.position, targetRange); 

    }

}
