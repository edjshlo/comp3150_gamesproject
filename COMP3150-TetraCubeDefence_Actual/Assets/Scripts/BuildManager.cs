﻿using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;

    //called right before start
    void Awake()
    {
        if (instance != null)
        {
            Debug.Log("more than one build manager in scene!");
        }

        instance = this;
    }

    public GameObject standardTurretPrefab;
    public GameObject longRangePrefab;
    public GameObject slowTowerPrefab;

    private TowerBlueprint turretToBuild;

    // public string planeTiltLeft = "planeLeft"; 

    public bool canBuild
    {
        get { return turretToBuild != null;  }
    }

    public bool hasMoney
    {
        get { return playerStats.money >= turretToBuild.cost; }
    }

    public void buildTowerOn(tetraNode node)
    {
        if (playerStats.money < turretToBuild.cost)
        {
            return;
        }

        playerStats.money -= turretToBuild.cost;

        GameObject tower = (GameObject) Instantiate(turretToBuild.prefab, node.getBuildPosition(), node.transform.rotation);
        node.tower = tower;

        Debug.Log("Money left: " + playerStats.money);

    }

    public void selectTurretToBuild(TowerBlueprint turret)
    {

        turretToBuild = turret;

    }

}
