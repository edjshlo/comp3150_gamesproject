﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoints : MonoBehaviour
{

	public static Transform[] EnemyWaypoints; 

	void Awake() { 

		EnemyWaypoints = new Transform[transform.childCount];

		for(int i = 0; i < EnemyWaypoints.Length; i++) { 

			EnemyWaypoints[i] = transform.GetChild(i);

		}

	}
}
