﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    public Transform[] cams;
    public float speed;
    Transform currentView;

    public string loadLevel = "DemoLevel";

    public SceneFader fader;
    public AudioFader audioFader;

    public Button[] levels;

    // Start is called before the first frame update
    void Start()
    {
        currentView = cams[0];
        Time.timeScale = 1f;

        int levelReached = PlayerPrefs.GetInt("levelReached", 1);

        for (int i = 0; i < levels.Length; i++)
        {
            if (i+1 > levelReached)
                levels[i].interactable = false;


        }
        //cams[0].GetComponent<Camera>();

        //cams[0].GetComponent<Camera>().enabled = true;
    }

    public void MainMenu()
    {
        currentView = cams[0];
    }

    public void StartLevel()
    {
        audioFader.AudioFadeTo();
        fader.FadeTo(loadLevel);
    }

    public void LevelSelect()
    {
        currentView = cams[1];
    }

    public void Options()
    {
        currentView = cams[2];
    }

    public void Quit()
    {
        Application.Quit();
    }

    void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, currentView.position, Time.deltaTime * speed);

        transform.rotation = Quaternion.Lerp(transform.rotation, currentView.rotation, Time.deltaTime * speed);
    }

}
