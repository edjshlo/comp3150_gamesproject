﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class FastForward : MonoBehaviour
{

	public bool fastforward = false; 

    // Update is called once per frame
    public void TogglefastForward(Image colourButton)
    {

    	// ColorBlock cb = colourButton.colors;

    	if(!fastforward) { 
    		colourButton.color = Color.red;
    		// cb.normalColor = Color.red;
    		Time.timeScale = 2.5f; 

    	} else { 
			colourButton.color = Color.black;
    		// cb.normalColor = Color.black;
    		Time.timeScale = 1.0f; 

    	}

    	fastforward = !fastforward; 
        
    }
}
