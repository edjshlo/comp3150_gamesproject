﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class frozoneTurret : MonoBehaviour
{

	public Transform target; 

    [Header("Turret Attributes")]
	public float targetRange = 10f; 
    public float fireRate = 1f; 
    private float countdownToFire = 0f;

    [Header("Unity Setup Fields")]
	public string enemyTag = "Enemy";
	//public Transform rotatingPart;
    public float rotateSpeed = 10f; 

    public GameObject turretBullet; 
    public Transform firingPoint;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    	if(target == null) { 

    		return; 

    	}

        //Turret lock on (turn on if needed really)
        // Vector3 direction = target.position - transform.position; 
        // Quaternion lookRotation = Quaternion.LookRotation(direction);
        // Vector3 Rotation = Quaternion.Lerp(rotatingPart.rotation, lookRotation, Time.deltaTime * rotateSpeed).eulerAngles;
        // rotatingPart.rotation = Quaternion.Euler(0f, Rotation.y, 0f); 

        if(countdownToFire <= 0f) { 

            //turretFire(); 
            countdownToFire = 1f/ fireRate; 

        }

        countdownToFire -= Time.deltaTime;

    }

    void OnDrawGizmosSelected() { 

    	Gizmos.color = Color.red; 
    	Gizmos.DrawWireSphere(transform.position, targetRange); 

    }
}
