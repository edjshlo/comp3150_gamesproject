using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{


    public static int liveEnemies = 0; 

    public Transform enemy1Prefab;
    // public Transform enemy2Prefab;
    public Wave [] waves;

    public Transform spawnPoint;

    public float countDownTimer = 5f; 
    //How long it takes to spawn the first wave
    private float countDown = 10f;

    private int waveNum = 0;

    public Text enemyWaveCountdown;
    public GameObject youWinOverlay;

    public string loadMenu = "MainMenu";
    public SceneFader fader;

    void Start() {
        liveEnemies = 0;
    } 

    void Update()
    {
        if(liveEnemies > 0) { 

            return; 

        }

        if (waveNum == waves.Length && liveEnemies == 0)
        {

            Debug.Log("LEVEL WON!");
            //gameEnded = true;
            youWinOverlay.SetActive(true);
            Time.timeScale = 0f;
            this.enabled = false;

            // if(liveEnemies != 0) {

            //     Time.timeScale = 0;
            //     youWinText.SetActive(true);

            // }
        }

        

        if (countDown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countDown = countDownTimer;
            return; 

        }
        countDown -= Time.deltaTime;

        countDown = Mathf.Clamp(countDown, 0f, Mathf.Infinity);  
        enemyWaveCountdown.text = string.Format("{0:00.00}", countDown);
    }

    public void RetryGameOver()
    {
        fader.FadeTo(SceneManager.GetActiveScene().name);
        Time.timeScale = 1f;
    }

    public void MainMenu()
    {
        fader.FadeTo(loadMenu);
        Time.timeScale = 1f;
    }

    IEnumerator SpawnWave()
    {
        
        Wave wave = waves[waveNum]; 
        liveEnemies+= waves[waveNum].count;  

        for (int i = 0; i < wave.count; i++)
        {
            SpawnEnemy(wave.enemyPrefab);
            yield return new WaitForSeconds(1f/ wave.spawnRate);
        }

        waveNum++;

        
        
    }
 
    void SpawnEnemy(GameObject enemy)
    {
        //Fix this so it spawns different enemies according to arrayspawner
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
        // Instantiate(enemy2Prefab, spawnPoint.position, spawnPoint.rotation);
        // liveEnemies++; 
        // Debug.Log(liveEnemies); 
    }

    
}
