﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

	private Transform target; 
	private int waypointIndex = 0; 

	public float startSpeed = 10f;

	[HideInInspector]
	public float EnemySpeed;   
	public int EnemyValue; 

	void Start() {

		EnemySpeed = startSpeed;
		target = Waypoints.EnemyWaypoints[0]; 

	}

	void Update() { 

		Vector3 movementDirection = target.position - transform.position; 
		transform.Translate(movementDirection.normalized * EnemySpeed * Time.deltaTime, Space.World); 

		if(Vector3.Distance(transform.position, target.position) <= 1f) { 

			GetNextWaypoint(); 

		}

		EnemySpeed = startSpeed; 

	}

	public void Slow(float amount) { 

		EnemySpeed = startSpeed * (1f - amount);

	}

	private void OnTriggerEnter(Collider other) {

			Debug.Log(other.tag);

	        //Check to see if the tag on the collider is equal to Enemy
	        if (other.tag == "TeleportationEntry") {

	        	Teleportation teleportLocation = other.GetComponent<Teleportation>(); 
				transform.position = teleportLocation.teleportExit.position;

				GetNextWaypoint();

				Debug.Log("Enemy teleported!");

	        }

	}

	void OnDestroy() { 

		playerStats.money += EnemyValue;

	}

	void GetNextWaypoint() { 

		if(waypointIndex >= Waypoints.EnemyWaypoints.Length - 1) {

			EndPath();
			return; 

		}

		waypointIndex++; 
		target = Waypoints.EnemyWaypoints[waypointIndex];

	}

	void EndPath()
    {
		playerStats.lives -= 5;
		Destroy(gameObject);
		WaveSpawner.liveEnemies--;

		if(playerStats.lives == 0) { 

			Time.timeScale = 0;

		}
	}	



}
