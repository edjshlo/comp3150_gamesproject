﻿using System.Collections;
using UnityEngine;

public class playerStats : MonoBehaviour
{
    //money
    public static int money;
    public int startMoney = 500;

    //lives
    public static int lives;
    public int startLives = 100;

    void Start()
    {
        money = startMoney;
        lives = startLives;
    }
}
