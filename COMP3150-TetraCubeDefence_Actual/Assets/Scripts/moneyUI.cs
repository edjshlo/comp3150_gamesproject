﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class moneyUI : MonoBehaviour
{
    public Text money;

    // Update is called once per frame
    void Update()
    {
        money.text = "$" + playerStats.money.ToString();
    }
}
